/**
 * @file main.cc
 * @author Matthieu Bucchianeri
 */

#include <stdlib.h>

#include <sphereplusplus/abort.hh>
#include <sphereplusplus/application.hh>
#include <sphereplusplus/gpio.hh>
#include <sphereplusplus/timer.hh>

#include <hw/mt3620.h>

/**
 * @brief A basic sample application, blinking an LED.
 */
class Basic : public Application
{
public:
    /**
     * @brief Constructor.
     */
    Basic() :
        Application(),
        m_blinky(MT3620_GPIO4, GPIO_OutputMode_PushPull),
        m_blinkyState(false),
        m_timer(*this)
    {
    }

    /**
     * @brief Destructor.
     */
    virtual ~Basic()
    {
    }

    /**
     * @brief Initialize the application.
     * @return True on success.
     */
    virtual bool init() final
    {
        AbortIfNot(Application::init(), false);

        AbortIfNot(m_blinky.init(true), false);

        AbortIfNot(m_timer.init(), false);
        m_timer.connect<Basic, &Basic::timerMethod>(*this);
        AbortIfNot(m_timer.startPeriodic(1000000), false);

        return true;
    }

private:
    /**
     * @brief The method invoked upon timer expiration.
     */
    void timerMethod()
    {
        m_blinkyState = !m_blinkyState;
        m_blinky.set(m_blinkyState);
    }

    /**
     * The GPIO for the LED the application controls.
     */
    GpioOut m_blinky;

    /**
     * The state of blinkiness of the LED.
     */
    bool m_blinkyState;

    /**
     * A periodic timer to blink the LED.
     */
    Timer m_timer;
};

int main(void)
{
    Basic app;
    AbortIfNot(app.init(), EXIT_FAILURE);
    AbortIfNot(app.run(), EXIT_FAILURE);

    return EXIT_SUCCESS;
}
