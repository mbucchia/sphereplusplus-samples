#  Copyright (c) Microsoft Corporation. All rights reserved.
#  Licensed under the MIT License.

cmake_minimum_required (VERSION 3.10)

project (basic C CXX)

azsphere_configure_tools(TOOLS_REVISION "20.07")
azsphere_configure_api(TARGET_API_SET "6")

# Setup use of C++.
add_compile_options(-std=c++14 -fno-exceptions -fno-non-call-exceptions -fno-rtti)

# Setup the Sphere++ library.
set(SPHERE_PLUS_PLUS_SOURCE
    sphereplusplus/abort.hh
    sphereplusplus/application.hh
    sphereplusplus/delegate.hh
    sphereplusplus/gpio.hh
    sphereplusplus/sphereplusplus.cc
    sphereplusplus/timer.hh)

# Create executable.
add_executable (${PROJECT_NAME}
                main.cc
                ${SPHERE_PLUS_PLUS_SOURCE})
target_include_directories (${PROJECT_NAME} PRIVATE .)
target_link_libraries (${PROJECT_NAME} applibs pthread gcc_s c)
azsphere_target_hardware_definition(${PROJECT_NAME} TARGET_DIRECTORY "hw" TARGET_DEFINITION "mt3620.json")

azsphere_target_add_image_package(${PROJECT_NAME})
